//
//  AppDelegate.swift
//  NuanceDemoApp
//
//  Created by Asad Ali on 24/12/2020.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        UserDefaults.standard.set(false, forKey: "_UIConstraintBasedLayoutLogUnsatisfiable")
        
        window?.overrideUserInterfaceStyle = .light
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    
    public func initilizeNuance(forUser userId: String) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {

            let appName = "Nebraska House Call Physician Group"
            let partnerGuid = "5eae6a92-1da2-454e-8f60-bff8de6bb92b"
            let licenseGuid = "43e4b971-d1bc-4114-ba7c-e4085fde13c8"
            
            NUSASession.shared().open(forApplication: appName,
                                      partnerGuid: partnerGuid,
                                      licenseGuid: licenseGuid,
                                      userId: userId)
        }
    }

}

