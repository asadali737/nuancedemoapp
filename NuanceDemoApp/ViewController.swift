//
//  ViewController.swift
//  NuanceDemoApp
//
//  Created by Asad Ali on 24/12/2020.
//

import UIKit

class ViewController: BaseViewController {

    
    @IBOutlet weak var btnLogin: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func btnLoginClicked(_ sender: Any) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.initilizeNuance(forUser: "imran")
        
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FirstViewController") as! FirstViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

