//
//  FourthViewController.swift
//  NuanceDemoApp
//
//  Created by Asad Ali on 24/12/2020.
//

import UIKit

class FourthViewController: BaseViewController {

    @IBOutlet weak var txtView1: UITextView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        txtView1.addBorder()
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        
        self.setupNuanceSpeechEverywhere()
    }
}
