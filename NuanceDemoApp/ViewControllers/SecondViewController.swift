//
//  SecondViewController.swift
//  NuanceDemoApp
//
//  Created by Asad Ali on 24/12/2020.
//

import UIKit

class SecondViewController: BaseViewController {

    
    @IBOutlet weak var txtView1: UITextView!
    @IBOutlet weak var txtView2: UITextView!
    @IBOutlet weak var txtView3: UITextView!
    @IBOutlet weak var txtView4: UITextView!
    @IBOutlet weak var txtView5: UITextView!
    @IBOutlet weak var txtView6: UITextView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        
        txtView1.addBorder()
        txtView2.addBorder()
        txtView3.addBorder()
        txtView4.addBorder()
        txtView5.addBorder()
        txtView6.addBorder()
        
        self.showRightNavigationBarButtonsWithTitle("Next")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        
        self.setupNuanceSpeechEverywhere()
    }

    override func navigationButtonClicked() {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ThirdViewController") as! ThirdViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnNextClicked(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ThirdViewController") as! ThirdViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
