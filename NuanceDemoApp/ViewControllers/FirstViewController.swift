//
//  FirstViewController.swift
//  NuanceDemoApp
//
//  Created by Asad Ali on 24/12/2020.
//

import UIKit

class FirstViewController: BaseViewController {

    @IBOutlet weak var btnNuanceView: UIButton!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        btnNuanceView.addBorder()
    }
    
    @IBAction func btnNuanceViewClicked(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SecondViewController") as! SecondViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
