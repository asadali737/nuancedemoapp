//
//  BaseViewController.swift
//  NuanceDemoApp
//
//  Created by Asad Ali on 24/12/2020.
//

import UIKit

extension UIView {
    
    func addBorder(_ borderColor: UIColor = UIColor.gray, borderWidth: CGFloat = 0.5) {
        
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
    }
}

class BaseViewController: UIViewController {

//    @IBOutlet var nuanceController: NUSAVuiController!
    
    open var nuanceController: NUSAVuiController!
    private(set) var isViewVisible: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        overrideUserInterfaceStyle = .light
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        print("view WillAppear - \(type(of: self))")
        
        if let _ = self.nuanceController {
            
            print("view nuanceController synchronize - \(type(of: self))")
            
            self.nuanceController.synchronizeWithView()
            self.nuanceController.delegate = self
            
            NUSASession.shared()?.delegate = self
            
            self.view.endEditing(true)
        }
        
        self.isViewVisible = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        print("view WillDisappear - \(type(of: self))")
        
        NUSASession.shared()?.abortRecording()
//        if let _ = self.nuanceController {
//
//            print("view nuanceController nil - \(type(of: self))")
//
//            self.nuanceController.delegate = nil
//            self.nuanceController = nil
//
//            NUSASession.shared()?.delegate = nil
//
//        }
        self.view.endEditing(true)
        
        self.isViewVisible = false
    }
    
    deinit {
        
        if self.nuanceController != nil {

            print("view nuanceController nil - \(type(of: self))")
            
            self.nuanceController.delegate = nil
            self.nuanceController = nil
            
            NUSASession.shared()?.delegate = nil
            
            self.view.endEditing(true)
        }
        
        NotificationCenter.default.removeObserver(self)
        print("view deinit - \(type(of: self))")
    }
    
    
    func setupNuanceSpeechEverywhere() {
        
        self.setupNuanceSpeechForView(view: self.view)
    }
    
    func setupNuanceSpeech() {
        
        self.nuanceController = nil
        
        self.setupNuanceSpeechForView(view: self.view)
        
        if self.nuanceController != nil {
            
            self.view.endEditing(false)
            self.nuanceController.synchronizeWithView()
            self.nuanceController.delegate = self
            
            NUSASession.shared()?.delegate = self
        }
    }
    
    func setupNuanceSpeechForView(view: UIView, isKeyboardVisibleByDefault: Bool = false) {
        
        if self.nuanceController == nil {
            
            print("view nuanceController init - \(type(of: self))")
            
            self.nuanceController = NUSAVuiController.init(view: view)
            self.nuanceController.delegate = self
            self.nuanceController.isKeyboardVisibleByDefault = isKeyboardVisibleByDefault
        }
        
        NUSASession.shared()?.delegate = self
    }

    
    func showRightNavigationBarButtonsWithTitle(_ title: String) {
            
        let btn = UIButton(type: .system)
        btn.setTitle(title, for: .normal)
        btn.addTarget(self, action: #selector(BaseViewController.rightNavigationButtonClicked(_:)), for: .touchUpInside)
        btn.sizeToFit()
        
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView:btn)]
    }
    
    @objc internal func rightNavigationButtonClicked(_ sender: UIButton) {
     
        self.navigationButtonClicked()
    }
    
    func navigationButtonClicked() {}
}



// MARK: - NUSAVuiControllerDelegate
extension BaseViewController: NUSASessionDelegate, NUSAVuiControllerDelegate {
    
    func vuiControllerDidStartProcessing() {
        
        print("vuiControllerDidStartProcessing: ", type(of: self))
    }
    
    func vuiControllerDidFinishProcessing() {
        
        print("vuiControllerDidFinishProcessing: ", type(of: self))
    }
    
    func sessionDidStartRecording() {
        
        print("sessionDidStartRecording: ", type(of: self))
    }
    
    func sessionDidStopRecording() {
        
        print("sessionDidStopRecording: ", type(of: self))
    }
}
