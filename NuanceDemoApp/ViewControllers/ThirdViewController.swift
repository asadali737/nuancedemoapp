//
//  ThirdViewController.swift
//  NuanceDemoApp
//
//  Created by Asad Ali on 24/12/2020.
//

import UIKit

class ThirdViewController: BaseViewController {

    @IBOutlet weak var txtView1: UITextView!
    @IBOutlet weak var txtView2: UITextView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        
        txtView1.addBorder()
        txtView2.addBorder()
        
        self.showRightNavigationBarButtonsWithTitle("Next")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        
        self.setupNuanceSpeechEverywhere()
    }

    override func navigationButtonClicked() {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FourthViewController") as! FourthViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnNextClicked(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FourthViewController") as! FourthViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
